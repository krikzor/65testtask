//
//  AddEditUserViewController.swift
//  65TestTask
//
//  Created by Antony on 25.09.2021.
//

protocol UserProtocol {
    func didTapSaveButton(user: User)
    func editDidTapSave(user: User)
}

import UIKit

class AddEditUserViewController: UIViewController {

    var user = User()
    var delegate: UserProtocol?
    var isEditingUser = false
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var companyTF: UITextField!
    @IBOutlet weak var dateOfBirthTF: UITextField!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var secondNameTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        saveButton.addTarget(self, action: #selector(didTappedSave), for: .touchUpInside)
        showCurrentUser()
        emailTF.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        saveButton.backgroundColor = .blue
        firstNameTF.delegate = self
        secondNameTF.delegate = self
        phoneNumberTF.delegate = self
        dateOfBirthTF.delegate = self
        companyTF.delegate = self
        emailTF.delegate = self
        saveButton.setTitle("Сохранить", for: .normal)
    }
    
    
    
    func saveButtonEnable() {
        saveButton.backgroundColor = .blue
        saveButton.setTitle("Сохранить", for: .normal)
        saveButton.isEnabled = true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if  isValidEmail(textField.text ?? "") {
            saveButtonEnable()
        } else {
            saveButton.backgroundColor = .gray
            saveButton.setTitle("Bad e-mail", for: .normal)
            saveButton.isEnabled = false
        }
    }
    
    @objc func didTappedSave() {
        
        if phoneNumberTF.text == "" || dateOfBirthTF.text == "" || companyTF.text == "" || emailTF.text == "" || firstNameTF.text == "" || secondNameTF.text == "" {
            saveButton.isEnabled = false
            saveButton.backgroundColor = .gray
            saveButton.setTitle("Все поля должны быть заполнены :)", for: .normal)
        } else {
            if isValidEmail(emailTF.text ?? "") == false {
                saveButton.backgroundColor = .gray
                saveButton.setTitle("Bad e-mail", for: .normal)
                saveButton.isEnabled = false
            } else {
                user.telephoneNumber = phoneNumberTF.text ?? ""
                user.dateOfBirth = dateOfBirthTF.text  ?? ""
                user.companyName = companyTF.text  ?? ""
                user.emailString = emailTF.text  ?? ""
                user.firstName = firstNameTF.text  ?? ""
                user.secondName = secondNameTF.text ?? ""
                if isEditingUser == true {
                    delegate?.editDidTapSave(user: user)
                } else {
                    delegate?.didTapSaveButton(user: user)
                }
               
                self.navigationController?.popToRootViewController(animated: true)
            }
             
           
        }
       
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func format(with mask: String, text: String) -> String {
        let numbers = text.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        var result = ""
        var index = numbers.startIndex
        for ch in mask where index < numbers.endIndex {
            if ch == "X" {
                result.append(numbers[index])

                index = numbers.index(after: index)

            } else {
                result.append(ch)
            }
        }
        return result
    }

    
    
    func showCurrentUser() {
        if isEditingUser == true {
            phoneNumberTF.text = user.telephoneNumber
            emailTF.text = user.emailString
            companyTF.text = user.companyName
            dateOfBirthTF.text = user.dateOfBirth
            firstNameTF.text = user.firstName
            secondNameTF.text = user.secondName
        }
    }

   

}

extension AddEditUserViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        saveButtonEnable()
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == secondNameTF || textField == firstNameTF {
            do {
                    let regex = try NSRegularExpression(pattern: ".*[^A-Za-zА-Яа-я ].*", options: [])
                    if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                        return false
                    }
                }
                catch {
                    print("smthng wrong")
                }
        }
        
        if textField == phoneNumberTF {
            guard let text = textField.text else { return false }
              let newString = (text as NSString).replacingCharacters(in: range, with: string)
              textField.text = format(with: "+X (XXX) XXX-XX-XX", text: newString)
              return false
        }
        
        if textField == dateOfBirthTF {
            guard let text = textField.text else { return false }
              let newString = (text as NSString).replacingCharacters(in: range, with: string)
              textField.text = format(with: "XX.XX.XXXX", text: newString)
              return false
        }
        return true
    }
}
