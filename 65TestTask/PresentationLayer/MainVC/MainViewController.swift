//
//  MainViewController.swift
//  65TestTask
//
//  Created by Antony on 25.09.2021.
//

import UIKit

class MainViewController: UIViewController {
    
    var tableView: UITableView  = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    var emptyLabel: UILabel = {
        let label = UILabel()
        label.text = "Список контактов пуст"
        label.font = .boldSystemFont(ofSize: 32)
        label.textColor = .red
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let searchController = UISearchController()
    var isFiltering: Bool {
        return searchController.isActive
    }
    
    
    var users = [User]()
    var filteredUsers = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupTableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "UsersTableViewCell", bundle: .main), forCellReuseIdentifier: "UsersTableViewCell")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addUser))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if users.isEmpty {
            usersIsEmpty()
        } else {
            self.navigationItem.searchController = searchController
            searchController.searchResultsUpdater = self
            tableView.isHidden = false
            emptyLabel.removeFromSuperview()
        }
    }
    
    func setupTableView() {
        self.view.addSubview(tableView)
        NSLayoutConstraint.activate([
                                        tableView.topAnchor.constraint(equalTo: self.view.topAnchor),
                                        tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
                                        tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
                                        tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),])
    }
    
    func usersIsEmpty() {
        
        self.tableView.isHidden = true
        self.view.addSubview(emptyLabel)
        NSLayoutConstraint.activate([
                                        emptyLabel.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
                                        emptyLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),])
    }
    
    
    
    
    @objc func addUser() {
        let vc = AddEditUserViewController()
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

//MARK: - TableViewDataSource & TableViewDelegate -
extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return filteredUsers.count
        }
        
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UsersTableViewCell", for: indexPath) as! UsersTableViewCell
        cell.backgroundColor = .red
        let user: User
        
        if isFiltering {
            user = filteredUsers[indexPath.row]
        } else {
            user = users[indexPath.row]
        }
        cell.configure(user: user)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentUser = users[indexPath.row]
        let vc = AddEditUserViewController()
        vc.user = currentUser
        vc.isEditingUser = true
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if isFiltering {
                filteredUsers.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .automatic)
            } else {
                users.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .automatic)
                if users.isEmpty {
                    usersIsEmpty()
                }
            }
        }
    }
    
    
}

//MARK: - UserDelegate-
extension MainViewController: UserProtocol {
    func editDidTapSave(user: User) {
        for (index,profile) in users.enumerated() {
            if profile.uuid == user.uuid {
                users.remove(at: index)
                users.append(user)
                tableView.reloadData()
            }
        }
    }
    
    func didTapSaveButton(user: User) {
        users.append(user)
        tableView.reloadData()
    }
}
// MARK: - SearchControllerDelegate-
extension MainViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else {return}
        
        filteredUsers = users.filter({ (user) -> Bool in
            user.firstName.lowercased().contains(searchText.lowercased()) || user.secondName.lowercased().contains(searchText.lowercased()) || user.fullName.lowercased().contains(searchText.lowercased()) || user.emailString.lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
        
    }
    
    
}
