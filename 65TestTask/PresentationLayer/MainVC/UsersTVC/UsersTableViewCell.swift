//
//  UsersTableViewCell.swift
//  65TestTask
//
//  Created by Antony on 25.09.2021.
//

import UIKit

class UsersTableViewCell: UITableViewCell {

    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    @IBOutlet weak var secondNameLabel: UILabel!
    @IBOutlet weak var firstNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(user: User) {
        phoneLabel.text = user.telephoneNumber
        emailLabel.text = user.emailString
        companyLabel.text = user.companyName
        dateOfBirthLabel.text = user.dateOfBirth
        secondNameLabel.text = user.secondName
        firstNameLabel.text = user.firstName
    }
    
}
