//
//  User.swift
//  65TestTask
//
//  Created by Antony on 25.09.2021.
//

import Foundation

struct User {
    var secondName: String = ""
    var firstName: String = ""
    var dateOfBirth: String = ""
    var companyName: String = ""
    var emailString: String = ""
    var telephoneNumber: String = ""
    var uuid: String = UUID().uuidString
    var fullName: String {
        firstName + " " + secondName
    }
    
}
